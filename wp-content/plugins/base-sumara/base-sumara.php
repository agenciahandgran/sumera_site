<?php
/**
 * Plugin Name: Base Sumera
 * Description: Controle base do tema Sumera.
 * Version: 0.1
 * Author: Hudson Caronilo
 * Author URI: 
 * Licence: GPL2
 */

	function baseSumera() {

		//TIPOS DE CONTEÚDO
		conteudosSumera();

		//TAXONOMIA
		taxonomiaSumera();

		//META BOXES
		metaboxesSumera();
	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/
		function conteudosSumera (){

			// TIPOS DE DESTQUE
			tipoDestaque();

			// TIPOS DE SERVIÇOS
			tipoServico();

			// TIPOS DE PROJETOS
			tipoProjetos();

			//PARCEIROS
			tipoParceiro();

			// EQUIPE
			tipoEquipe();

			//DEPOIMENTOS
			tipoDepoimento();

			/* ALTERAÇÃO DO TÍTULO PADRÃO */
			add_filter( 'enter_title_here', 'trocarTituloPadrao' );
			function trocarTituloPadrao($titulo){

				switch (get_current_screen()->post_type) {

					case 'equipe':
						$titulo = 'Nome do integrante';
					break;

					default:
					break;
				}
			    return $titulo;
			}

		}
	
	/****************************************************
	* CUSTOM POST TYPE
	*****************************************************/

		// CUSTOM POST TYPE DESTAQUES
		function tipoDestaque() {

			$rotulosDestaque = array(
									'name'               => 'Destaque',
									'singular_name'      => 'destaque',
									'menu_name'          => 'Destaques',
									'name_admin_bar'     => 'Destaques',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo destaque',
									'new_item'           => 'Novo destaque',
									'edit_item'          => 'Editar destaque',
									'view_item'          => 'Ver destaque',
									'all_items'          => 'Todos os destaque',
									'search_items'       => 'Buscar destaque',
									'parent_item_colon'  => 'Dos destaque',
									'not_found'          => 'Nenhum destaque cadastrado.',
									'not_found_in_trash' => 'Nenhum destaque na lixeira.'
								);

			$argsDestaque 	= array(
									'labels'             => $rotulosDestaque,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-megaphone',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'destaque' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail','editor')
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('destaque', $argsDestaque);

		}

		// CUSTOM POST TYPE SERVIÇO
		function tipoServico() {
			$rotulosServico = array(
				'name'               => 'Serviços',
				'singular_name'      => 'Serviço',
				'menu_name'          => 'Serviços',
				'name_admin_bar'     => 'Serviços',
				'add_new'            => 'Adicionar novo',
				'add_new_item'       => 'Adicionar novo serviço',
				'new_item'           => 'Novo serviço',
				'edit_item'          => 'Editar serviço',
				'view_item'          => 'Ver serviço',
				'all_items'          => 'Todos os serviços',
				'search_items'       => 'Buscar serviços',
				'parent_item_colon'  => 'Dos serviços',
				'not_found'          => 'Nenhum serviço cadastrado.',
				'not_found_in_trash' => 'Nenhum serviço na lixeira.'
			);

			$argsServico 	= array(
				'labels'             => $rotulosServico,
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => true,
				'menu_position'		 => 4,
				'menu_icon'          => 'dashicons-admin-tools',
				'query_var'          => true,
				'rewrite'            => array( 'slug' => 'servicos' ),
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'supports'           => array( 'title', 'thumbnail', 'editor' )
			);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('servico', $argsServico);

		}

		// CUSTOM POST TYPE PROJETOS
		function tipoProjetos() {
			$rotulosProjetos = array(
				'name'               => 'Projeto',
				'singular_name'      => 'projeto',
				'menu_name'          => 'Projetos',
				'name_admin_bar'     => 'projetos',
				'add_new'            => 'Adicionar projeto',
				'add_new_item'       => 'Adicionar novo projeto',
				'new_item'           => 'Novo projeto',
				'edit_item'          => 'Editar projeto',
				'view_item'          => 'Ver projeto',
				'all_items'          => 'Todos os projetos',
				'search_items'       => 'Buscar projeto',
				'parent_item_colon'  => 'Dos projetos',
				'not_found'          => 'Nenhum projeto cadastrado.',
				'not_found_in_trash' => 'Nenhum projeto na lixeira.'

			);
			$argsProjetos 	= array(
				'labels'             => $rotulosProjetos,
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => true,
				'menu_position'		 => 4,
				'menu_icon'          => 'dashicons-welcome-widgets-menus',
				'query_var'          => true,
				'rewrite'            => array( 'slug' => 'projetos' ),
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'supports'           => array( 'title', 'thumbnail','editor')
			);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('projetos', $argsProjetos);

		}

		// CUSTOM POST TYPE PARCEIRO
		function tipoParceiro() {

			$rotulosParceiro = array(
				'name'               => 'Clientes',
				'singular_name'      => 'parceiro',
				'menu_name'          => 'Clientes',
				'name_admin_bar'     => 'Clientes',
				'add_new'            => 'Adicionar novo',
				'add_new_item'       => 'Adicionar novo parceiro',
				'new_item'           => 'Novo cliente',
				'edit_item'          => 'Editar cliente',
				'view_item'          => 'Ver cliente',
				'all_items'          => 'Todos os clientes',
				'search_items'       => 'Buscar clientes',
				'parent_item_colon'  => 'Dos clientes',
				'not_found'          => 'Nenhum cliente cadastrado.',
				'not_found_in_trash' => 'Nenhum cliente na lixeira.'
			);

			$argsParceiro 	= array(
				'labels'             => $rotulosParceiro,
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => true,
				'menu_position'		 => 4,
				'menu_icon'          => 'dashicons-universal-access',
				'query_var'          => true,
				'rewrite'            => array( 'slug' => 'clientes' ),
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'supports'           => array( 'title', 'thumbnail')

			);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('parceiros', $argsParceiro);
		}

		// CUSTOM POST TYPE COLABORADOR
		function tipoEquipe() {

			$rotulosEquipe = array(
									'name'               => 'especialista',
									'singular_name'      => 'especialista',
									'menu_name'          => 'Especialista',
									'name_admin_bar'     => 'especialista',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo membro',
									'new_item'           => 'Novo membro',
									'edit_item'          => 'Editar membro',
									'view_item'          => 'Ver membro',
									'all_items'          => 'Todos os membros',
									'search_items'       => 'Buscar membros',
									'parent_item_colon'  => 'Dos membros',
									'not_found'          => 'Nenhum membro cadastrado.',
									'not_found_in_trash' => 'Nenhum membro na lixeira.'
								);

			$argsEquipe 	= array(
									'labels'             => $rotulosEquipe,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-nametag',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'equipe' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail','editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('equipe', $argsEquipe);

		}

		// CUSTOM POST TYPE DEPOIMENTO
		function tipoDepoimento() {

			$rotulosDepoimento = array(
									'name'               => 'Depoimentos',
									'singular_name'      => 'Depoimento',
									'menu_name'          => 'Depoimentos',
									'name_admin_bar'     => 'Depoimentos',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo depoimento',
									'new_item'           => 'Novo depoimento',
									'edit_item'          => 'Editar depoimento',
									'view_item'          => 'Ver depoimento',
									'all_items'          => 'Todos os depoimentos',
									'search_items'       => 'Buscar depoimentos',
									'parent_item_colon'  => 'Dos depoimentos',
									'not_found'          => 'Nenhum depoimento cadastrado.',
									'not_found_in_trash' => 'Nenhum depoimento na lixeira.'
								);

			$argsDepoimento 	= array(
									'labels'             => $rotulosDepoimento,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-heart',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'depoimento' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('depoimento', $argsDepoimento);

		}
		
	/****************************************************
	* META BOXES
	*****************************************************/
		function metaboxesSumera(){
			add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );
		}

			function registraMetaboxes( $metaboxes ){

				// PREFIX
				$prefix = 'Sumera_';

				// METABOX COMO DESTAQUES
				$metaboxes[] = array(
					'id'			=> 'metaboxServico',
					'title'			=> 'Detalhes do servico',
					'pages' 		=> array('servico'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
						array(
							'name'  => 'Breve descrição: ',
							'id'    => "{$prefix}servico_breveDescricao",
							'desc'  => '',
							'type'  => 'textarea',
						), 
						array(
							'name'  => 'Ícone do serviço: ',
							'id'    => "{$prefix}servico_icone",
							'desc'  => '',
							'type'  => 'image_advanced',
						),
					),
				);

				// METABOX EQUIPE
				$metaboxes[] = array(
					'id'			=> 'metaboxEquipe',
					'title'			=> 'Detalhes do integrante',
					'pages' 		=> array('equipe'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
						array(
							'name'  => 'Linkdin: ',
							'id'    => "{$prefix}equipe_redeSolcial",
							'desc'  => '',
							'type'  => 'text',
						),
						array(
							'name'  => 'Cargo: ',
							'id'    => "{$prefix}equipe_cargo",
							'desc'  => '',
							'type'  => 'text',
						), 
					),
				);

				// METABOX EQUIPE
				$metaboxes[] = array(
					'id'			=> 'metaboxProjeto',
					'title'			=> 'Detalhes do projeto',
					'pages' 		=> array('projetos'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
						array(
							'name'  => 'Galeria: ',
							'id'    => "{$prefix}projetos_galeria",
							'desc'  => '',
							'type'  => 'image_advanced',
						),
					),
				);

				return $metaboxes;
			}

	/****************************************************
	* TAXONOMIA
	*****************************************************/
		function taxonomiaSumera () {
			// taxonomiaCategoriaProjetos();
		}

		// function taxonomiaCategoriaProjetos() {

		// 	$rotulosCategoriaProjetos = array(
		// 										'name'              => 'Categorias de projeto',
		// 										'singular_name'     => 'Categorias de projetos',
		// 										'search_items'      => 'Buscar categoria do projeto',
		// 										'all_items'         => 'Todas as categorias',
		// 										'parent_item'       => 'Categoria pai',
		// 										'parent_item_colon' => 'Categoria pai:',
		// 										'edit_item'         => 'Editar categoria do projeto',
		// 										'update_item'       => 'Atualizar categoria',
		// 										'add_new_item'      => 'Nova categoria',
		// 										'new_item_name'     => 'Nova categoria',
		// 										'menu_name'         => 'Categorias projetos',
		// 									);

		// 	$argsCategoriaProjetos 		= array(
		// 										'hierarchical'      => true,
		// 										'labels'            => $rotulosCategoriaProjetos,
		// 										'show_ui'           => true,
		// 										'show_admin_column' => true,
		// 										'query_var'         => true,
		// 										'rewrite'           => array( 'slug' => 'categoria-projetos' ),
		// 									);

		// 	register_taxonomy( 'categoriaProjetos', array( 'projetos' ), $argsCategoriaProjetos);

		// }
		// 
		


		function metaboxjs(){



			global $post;

			$template = get_post_meta($post->ID, '_wp_page_template', true);

			$template = explode('/', $template);

			$template = explode('.', $template[1]);

			$template = $template[0];



			if($template != ''){

				wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);

			}

		}


  	/****************************************************
	* AÇÕES
	*****************************************************/

		// INICIA A FUNÇÃO PRINCIPAL
		add_action('init', 'baseSumera');

		// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
		add_action( 'add_meta_boxes', 'metaboxjs');

		// FLUSHS
		function rewrite_flush() {

	    	baseSumera();

	   		flush_rewrite_rules();
		}

		register_activation_hook( __FILE__, 'rewrite_flush' );