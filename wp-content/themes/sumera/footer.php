<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Sumera
 */
global $configuracao;
?>
<div class="section-8">
  <div class="div-menu-footer">
    <div class="row-9 w-row">
      
      <div class="w-col w-col-6">
        <img src="<?php echo $configuracao['opt_logoRodape']['url'] ?>">
      </div>
       <div class="w-col w-col-6">
          <div class="navRodape">
            <a href="<?php echo home_url('/empresa/'); ?>" class="link">EMPRESA</a>
            <a href="<?php echo home_url('/servicos/'); ?>" class="link">Serviços</a>
            <a href="<?php echo home_url('/projetos/'); ?>" class="link">Projetos</a>
            <a href="<?php echo home_url('/contato/'); ?>" class="link">contato</a>
          </div>
        </div>
    </div>

<style>
  .navRodape{
    text-align: right;
    
  }
   .navRodape a{
       margin-left: 50px;

  }

  .contatoResponsivo{

  }
      .contatoResponsivo h1{
        color: #fff;
        font-size: 27px;
        text-align: center;
      }
      .contatoResponsivo p{
        color: #fff;
        text-align: center;
      }
</style>

    <div class="row-5 hiddenFooter w-row">
      <div class="w-col w-col-3">
        <div class="text-block">© <?php echo $configuracao['opt_Copyryght'] ?></div>
      </div>
      <div class="column-21 w-col w-col-4">
        <h1 class="heading-emprea"><?php echo $configuracao['opt_contato'] ?></h1>
      </div>
      <div class="w-col w-col-3">
        <div class="text-block-2"><?php echo $configuracao['opt_endereco'] ?></div>
      </div>
      <div class="column-23 w-col w-col-1">
        <?php if ($configuracao['opt_face']): ?>
          <a href="<?php echo $configuracao['opt_face'] ?>">
            <img src="<?php bloginfo('template_directory'); ?>/images/facebook-icon.png">
          </a>
        <?php endif; ?>
        </div>
        <div class="column-22 w-col w-col-1">
            <?php if ($configuracao['opt_insta']): ?>
          <a href="<?php echo $configuracao['opt_insta'] ?>">
            <img src="<?php bloginfo('template_directory'); ?>/images/instagram-icon-.png">
          </a>
           <?php endif; ?>
        </div>
    </div>
    <div class="contatoResponsivo"> 
    
    <h1> <?php echo $configuracao['opt_contato'] ?></h1>
     <p><?php echo $configuracao['opt_endereco'] ?></p>
      <p>© <?php echo $configuracao['opt_Copyryght'] ?></p>

  </div>
  </div>


</div>
  

<?php wp_footer(); ?>

</body>
</html>
