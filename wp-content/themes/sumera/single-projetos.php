<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Sumera
 */

get_header(); ?>
<div class="section-projeto-selecionado">
    <div class="div-projeto-selecionado">
      <h1 class="heading-23"><?php echo get_the_title() ?></h1>
      <div data-animation="slide" data-duration="500" data-infinite="1" class="slider-2 w-slider">
        <div class="w-slider-mask">
         
            <?php 
                $galeria  = rwmb_meta('Sumera_projetos_galeria');
                  foreach($galeria as $galeria):
                   $galeria = $galeria['full_url'];
                      
              ?>
               <div class="w-slide">
            <img src="<?php echo $galeria; ?>" srcset="images/img-galeria-p-500.png 500w, <?php echo $galeria; ?> 750w" sizes="(max-width: 750px) 100vw, 750px">
              </div>
            <?php endforeach; ?>
       
         
        </div>
        <div class="w-slider-arrow-left">
          <div class="w-icon-slider-left"></div>
        </div>
        <div class="w-slider-arrow-right">
          <div class="w-icon-slider-right"></div>
        </div>
        <div class="w-slider-nav w-round"></div>
      </div>
      <h1 class="heading-3">Sobre</h1>
      <div class="textoProjetos">
      <?php echo the_content() ?>
      </div>
    </div>
  </div>
<?php
get_footer();
