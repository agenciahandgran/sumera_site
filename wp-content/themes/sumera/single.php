<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Sumera
 */
 $fotoBlog = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
  $fotoBlog = $fotoBlog[0];
get_header(); ?>

	<div class="section-projeto-selecionado">
    <div class="div-projeto-selecionado">
      <h1 class="heading-23"><?php echo get_the_title() ?></h1>
    
        
         
        		<img src="<?php echo  $fotoBlog; ?>" class="imgPOst">

       
     
      <div class="textoBLog">
      <?php echo the_content() ?>
      </div>
    </div>
  </div>
<style>
	.textoBLog{
		box-sizing: border-box;
		margin-top: 40px;
		margin-bottom: 40px;
		font-family: Montserrat, sans-serif;
		color: #969696;
		line-height: 26px;
	}
		.textoBLog p{
			color: #969696;
			line-height: 26px;
			font-family: Montserrat, sans-serif;
		}
		.imgPOst{
			display: block;
			margin: 0 auto;
			max-width: 750px;
			border: solid 1px #002c50;
		}
</style>
<?php

get_footer();
