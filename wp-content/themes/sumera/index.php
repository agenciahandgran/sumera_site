<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Sumera
 */

get_header(); ?>

	<div class="section-header-projetos">
    <div class="div-header-projetos">
      <div class="w-row">
        <div class="column-35 w-col w-col-6">
         <?php echo $configuracao['blogs_texto_Descricao'] ?>
        </div>
        <div class="w-col w-col-6">
          <img src="<?php echo $configuracao['img_blog_ilustrativa']['url'] ?>" srcset="<?php echo $configuracao['img_blog_ilustrativa']['url'] ?>, <?php echo $configuracao['img_blog_ilustrativa']['url'] ?> 602w" sizes="(max-width: 479px) 100vw, (max-width: 767px) 47vw, 48vw">
        </div>
      </div>
    </div>
  </div>

  <div class="section-projetos-lista">
    <div class="div-projetos-lista">
      <div class="row-10 w-row">
                <?php 
          //LOOP DE POST PROJETOS
          $custonProjetos = new WP_Query( array( 'post_type' => 'post', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
          while ( $custonProjetos->have_posts() ) : $custonProjetos->the_post();
              $fotoProjetos = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
              $fotoProjetos = $fotoProjetos[0];
              $servico_icone = rwmb_meta('Sumera_servico_icone');
        ?>
        

          <div class="w-col w-col-4">
            <div class="correcaoMargin">
            <a href="<?php echo get_permalink() ?>" class="w-inline-block" style="    max-width: 100%;display: inline;">
              <figure class="fotoPorjeto" style="background: url(<?php echo $fotoProjetos ?>);    display: block;width: 100%;max-width: 360px;height: 209px;background-position: center!important; background-size: cover!important;">
              </figure>
            </a>
            <h1 class="heading-3"><?php echo get_the_title() ?></h1>
            <p class="paragraph-projetos"><?php customExcerpt(100); ?></p>
            <a href="<?php echo get_permalink() ?>" class="link-2">Ver postagem</a>
           </div>
           </div>
         
         <?php  endwhile; wp_reset_query(); ?>
       
       
      </div>
    </div>
  </div>

<?php

get_footer();
