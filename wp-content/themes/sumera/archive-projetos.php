<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Sumera
 */

get_header(); ?>

<div class="section-header-projetos">
    <div class="div-header-projetos">
      <div class="w-row">
        <div class="column-35 w-col w-col-6">
         <?php echo $configuracao['projetros_texto_Descricao'] ?>
        </div>
        <div class="w-col w-col-6">
          <img src="<?php echo $configuracao['img_projetros_ilustrativa']['url'] ?>" srcset="<?php echo $configuracao['img_projetros_ilustrativa']['url'] ?>, <?php echo $configuracao['img_projetros_ilustrativa']['url'] ?> 602w" sizes="(max-width: 479px) 100vw, (max-width: 767px) 47vw, 48vw">
        </div>
      </div>
    </div>
  </div>

  <div class="section-projetos-lista">
    <div class="div-projetos-lista">
      <div class="row-10 w-row">
        <?php 
          //LOOP DE POST PROJETOS
          $custonProjetos = new WP_Query( array( 'post_type' => 'projetos', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
          while ( $custonProjetos->have_posts() ) : $custonProjetos->the_post();
              $fotoProjetos = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
              $fotoProjetos = $fotoProjetos[0];
              $servico_icone = rwmb_meta('Sumera_servico_icone');
        ?>
        

          <div class="w-col w-col-4">
            <div class="correcaoMargin">
            <a href="<?php echo get_permalink() ?>" class="w-inline-block" style="max-width: 100%;display: inline;">
              <figure class="fotoPorjeto" style="background: url(<?php echo $fotoProjetos ?>);    display: block;width: 100%;height: 209px;background-position: center!important; background-size: cover!important;">
              </figure>
            </a>
            <h1 class="heading-3"><?php echo get_the_title() ?></h1>
            <p class="paragraph-projetos"><?php customExcerpt(100); ?></p>
            <a href="<?php echo get_permalink() ?>" class="link-2">Ver Projeto</a>
           </div>
           </div>
         
         <?php  endwhile; wp_reset_query(); ?>
       
       
      </div>
    </div>
  </div>


  <div class="section-comments">
    <div class="div-comments">
      <h1 class="heading-7">Depoimentos</h1>
      <div class="w-row">
         
          <div class="column-33 w-col w-col-6">
          
           <?php 
            $i = 0;
              //LOOP DE POST PROJETOS
              $custonDepoimentos = new WP_Query( array( 'post_type' => 'depoimento', 'orderby' => 'id', 'order' => 'rand', 'posts_per_page' => 1) );
              while ( $custonDepoimentos->have_posts() ) : $custonDepoimentos->the_post();
                  $fotoDepoimento = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
                  $fotoDepoimento = $fotoDepoimento[0];
            ?>
            
            <img src="<?php echo  $fotoDepoimento ?>" class="image-8">
            <div class="div-block-10">
              <img src="<?php bloginfo('template_directory'); ?>/images/icon-comment.png">
            </div>
              <p class="paragraph"><?php echo get_the_content() ?></p>
              <h1 class="heading-19"><?php echo get_the_title() ?></h1>
            <?php  endwhile; wp_reset_query(); ?>
              
             
          </div>
       
       
        <div class="column-34 w-col w-col-6">
          
           <?php 
            $i = 0;
              //LOOP DE POST PROJETOS
              $custonDepoimentos = new WP_Query( array( 'post_type' => 'depoimento', 'orderby' => 'id', 'order' => 'rand', 'posts_per_page' => 1) );
              while ( $custonDepoimentos->have_posts() ) : $custonDepoimentos->the_post();
                  $fotoDepoimento = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
                  $fotoDepoimento = $fotoDepoimento[0];
            ?>
            <img src="<?php echo  $fotoDepoimento ?>">
            <div class="div-block-10">
              <img src="<?php bloginfo('template_directory'); ?>/images/icon-comment.png">
            </div>
              <p class="paragraph"><?php echo get_the_content() ?></p>
              <h1 class="heading-19"><?php echo get_the_title() ?></h1>
            <?php  endwhile; wp_reset_query(); ?>
        </div>
        
      </div>
    </div>
  </div>
<?php
get_footer();
