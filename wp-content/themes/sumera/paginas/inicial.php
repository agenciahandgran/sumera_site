<?php
/**
 * Template Name: Inicial
 * Description: Página inicial
 *
 * @package Sumera
 */

global $configuracao;

get_header(); ?>
	
  <div class="section">
    <div class="countainer3"></div>
  </div>

  <!-- ÁREA DE SERVIÇOS -->
  <div class="section-2">
    <div class="container1-servicos">
      <div class="div-block">
        <h1 class="heading-emprea">Serviços</h1>
        
        <?php 
            $i = 0;
            //LOOP DE POST Servico
            $posServico = new WP_Query( array( 'post_type' => 'servico', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => 3) );
            while ( $posServico->have_posts() ) : $posServico->the_post();
              if ($i == 1):

          ?>
          <h1 class="heading-5"><?php echo get_the_title() ?></h1>
          <p class="paragraph"><?php echo $servico_breveDescricao = rwmb_meta('Sumera_servico_breveDescricao'); ?></p>
         <?php else: ?>


          <h1 class="heading-4"><?php echo get_the_title() ?></h1>
          <p class="paragraph"><?php echo $servico_breveDescricao = rwmb_meta('Sumera_servico_breveDescricao'); ?></p>
        <?php   endif;$i++; endwhile; wp_reset_query(); ?>

        <a href="<?php echo home_url('/servicos/'); ?>" class="button w-button">Mais Serviços</a>

      </div>
      <img src="<?php echo $configuracao['img_servico']['url'] ?>" srcset="<?php echo $configuracao['img_servico']['url'] ?>, <?php echo $configuracao['img_servico']['url'] ?>, <?php echo $configuracao['img_servico']['url'] ?>, <?php echo $configuracao['img_servico']['url'] ?> 1240w" sizes="(max-width: 1240px) 100vw, 1240px">
    </div>
  </div>

  <!-- FUNDAMENTOS SUMARA -->
  <div class="section-3">
    <div class="container5">
      <h1 class="heading-7">Fundamentos Sumera</h1>
      <div class="w-row">

        <!-- FUNDAMENTOS -->
        <div class="column-3 w-col w-col-4">
          <div class="container w-container"><img src="<?php echo $configuracao['primeiro_icone']['url'] ?>"></div>
          <h1 class="heading-8"><?php echo $configuracao['primeiro_titulo']?></h1>
          <p class="paragraph-2"><?php echo $configuracao['primeiro_descricao']?></p>
        </div>

        <!-- FUNDAMENTOS -->
        <div class="column-4 w-col w-col-4">
          <div class="container w-container"><img src="<?php echo $configuracao['segundo_icone']['url'] ?>"></div>
          <h1 class="heading-8"><?php echo $configuracao['segundo_titulo']?></h1>
          <p class="paragraph-2"><?php echo $configuracao['segundo_descricao']?></p>
        </div>

        <!-- FUNDAMENTOS -->
        <div class="column-5 w-col w-col-4">
          <div class="container w-container"><img src="<?php echo $configuracao['terceiro_icone']['url'] ?>"></div>
          <h1 class="heading-8"><?php echo $configuracao['terceiro_titulo']?></h1>
          <p class="paragraph-2"><?php echo $configuracao['terceiro_descricao']?></p>
        </div>

      </div>
    </div>
  </div>
  
  <!-- INFORMATIVO -->
  <?php if ($configuracao['img_informativo']['url']):?>
  <div class="section-4" style="background:url(<?php echo $configuracao['img_informativo']['url'] ?>)">
    <div class="container1-caixa">
      <div class="row-2 w-row">
        <div class="w-col w-col-8">
          <h1 class="heading-9" style="display: none">O sonho da casa própria<br>ao seu alcance.</h1>
          <h1 class="heading-10" style="display: none">CONSTRUA COM A SUMERA E PAGUE EM ATÉ 360X</h1>
        </div>
        <!-- <div class="w-col w-col-4"><img src="images/logo-caixa-financiamento.png"></div> -->
      </div>
    </div>
  </div>
<?php endif; ?>

  <!-- ÁREA DE PROJETOS  -->
  <div class="section-5">
    <div class="container-projetos">
      <h1 class="heading-7">Projetos Recentes</h1>
      <div class="w-row">
        
         <?php 
            $i = 0;
            //LOOP DE POST Servico
            $projetos = new WP_Query( array( 'post_type' => 'projetos', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => 2) );
            while ( $projetos->have_posts() ) : $projetos->the_post();
                $fotoProjeto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
                $fotoProjeto = $fotoProjeto[0];
            if ($i == 0):

          ?>
        <!-- PROJETO -->
        <div class="column-15 w-col w-col-6">
          <div class="div-block-4">
            <h1 class="heading-11"><?php echo get_the_title() ?></h1>
            <a href="<?php echo get_permalink() ?>" class="link-block w-inline-block">
              <div class="row-4 w-row">
                <div class="column-6 w-col w-col-8">
                  <h1 class="heading-13">Leia Mais</h1>
                </div>
                <div class="column-7 w-col w-col-4"><img src="<?php bloginfo('template_directory'); ?>/images/icon-lus.png"></div>
              </div>
            </a>
          </div>
          <img src="<?php echo $fotoProjeto ?>">
        </div>
        
        <?php else: ?>

        <!-- PROJETO -->
        <div class="w-col w-col-6">
          <div class="div-block-4">
            <h1 class="heading-11"><?php echo get_the_title() ?></h1>
            <a href="<?php echo get_permalink() ?>" class="link-block w-inline-block">
              <div class="row-4 w-row">
                <div class="column-6 w-col w-col-8">
                  <h1 class="heading-13">Leia Mais</h1>
                </div>
                <div class="column-7 w-col w-col-4"><img src="<?php bloginfo('template_directory'); ?>/images/icon-lus.png"></div>
              </div>
            </a>
          </div>
          <img src="<?php echo $fotoProjeto ?>">
        </div>

        <?php endif;$i++; endwhile; wp_reset_query(); ?>

      </div>
    </div>
  </div>

  <!-- PARCEIROS -->
  <div class="section-6">
    <div class="div-parceiros">
      <h1 class="heading-7">Clientes</h1>
      <div class="w-row">
        <?php 
        $i = 0;
        //LOOP DE POST PARCEIROS
        $parceiros = new WP_Query( array( 'post_type' => 'parceiros', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => 4) );
        while ( $parceiros->have_posts() ) : $parceiros->the_post();
            $fotoParceiros = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
            $fotoParceiros = $fotoParceiros[0];
        if ($i == 0){

      ?>
        <div class="column-11 w-col w-col-12">
            <img src="<?php echo $fotoParceiros ?>" style="max-width: 175px;">
          </div>
        <?php }else if($i == 1){ ?>
        <div class="column-10 w-col w-col-12">
          <img src="<?php echo $fotoParceiros ?>" style="max-width: 175px;">
        </div>
        <?php }else if($i == 2){ ?>
        <div class="column-9 w-col w-col-12">
          <img src="<?php echo $fotoParceiros ?>" style="max-width: 175px;">
        </div>
        <?php }else if($i == 3){ ?>
        <div class="column-8 w-col w-col-12">
          <img src="<?php echo $fotoParceiros ?>" style="max-width: 175px;">
        </div>
       <?php };$i++; endwhile; wp_reset_query(); ?>
      </div>
    </div>
  </div>

  <div class="areaContoato">
  <div class="section-contato">
    <div class="div-contato">
      <h1>Entre em contato</h1>
      <div class="w-form">

        <?php echo do_shortcode('[contact-form-7 id="12" title="Formulário de contato"]'); ?>
      

        <div class="w-form-done">
          <div>Thank you! Your submission has been received!</div>
        </div>
        <div class="w-form-fail">
          <div>Oops! Something went wrong while submitting the form.</div>
        </div>
      </div>
    </div>
  </div>
  </div>
  <style>
   
  </style>

<?php get_footer(); ?>