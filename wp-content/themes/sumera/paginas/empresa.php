<?php
/**
 * Template Name: Empresa
 * Description: Página Empresa
 *
 * @package Sumera
 */

global $configuracao;

get_header(); ?>
<div class="section-img-empresa" style="background:url(<?php echo $configuracao['banner_empresa']['url'] ?>)">
	<div class="div-title-header">
		<h1 class="heading-20"><?php echo $configuracao['banner_texto'] ?></h1>
	</div>
</div>	

<div class="section-nossa-historia">
    <div class="div-nossa-historia">
      <div class="row-6 w-row">
        <div class="w-col w-col-6">
         
         <?php echo $configuracao['texto'] ?>
        </div>
        <div class="w-col w-col-6"><img src="<?php echo $configuracao['imgIlustrativa_empresa']['url'] ?>" class="image-3"></div>
      </div>
    </div>
  </div>

   <div class="section-features">
    <div class="div-feature">
      <h1 class="heading-emprea-3">Fundamentos Sumera</h1>
      <div class="w-row">
        <div class="column-27 w-col w-col-4"><img src="<?php echo $configuracao['primeiro_icone']['url'] ?>">
          <h1 class="heading-17"><?php echo $configuracao['primeiro_titulo']?></h1>
          <p class="paragraph-2"><?php echo $configuracao['primeiro_descricao']?></p>
        </div>
        <div class="column-28 w-col w-col-4"><img src="<?php echo $configuracao['segundo_icone']['url'] ?>">
          <h1 class="heading-17"><?php echo $configuracao['segundo_titulo']?></h1>
          <p class="paragraph-2"><?php echo $configuracao['segundo_descricao']?></p>
        </div>
        <div class="column-29 w-col w-col-4"><img src="<?php echo $configuracao['terceiro_icone']['url'] ?>">
          <h1 class="heading-17"><?php echo $configuracao['terceiro_titulo']?></h1>
          <p class="paragraph-2"><?php echo $configuracao['terceiro_descricao']?></p>
        </div>
      </div>
    </div>
  </div>

  <div class="div-missao">
    <div class="div-mvv">
      <div class="w-row">
        <div class="w-col w-col-4">
          <h1 class="heading-3"><?php echo $configuracao['missao']?></h1>
          <p class="paragraph"><?php echo $configuracao['missaoTexto']?></p>
        </div>
        <div class="w-col w-col-4">
          <h1 class="heading-3"><?php echo $configuracao['visao']?></h1>
          <p class="paragraph"><?php echo $configuracao['visaoTexto']?></p>
        </div>
        <div class="w-col w-col-4">
          <h1 class="heading-3"><?php echo $configuracao['valores']?></h1>
          <p class="paragraph"><?php echo $configuracao['valoresTexto']?></p>
        </div>
      </div>
    </div>
  </div>

  <div class="section-team">
    <div class="div-features">
      <h1 class="heading-7">Conheça nossa Equipe</h1>
      <div class="w-row">
       <?php 
          $i = 0;
          //LOOP DE POST Servico
          $equipe = new WP_Query( array( 'post_type' => 'equipe', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => 3) );
          while ( $equipe->have_posts() ) : $equipe->the_post();
              $fotoEquipe = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
              $fotoEquipe = $fotoEquipe[0];
          if ($i == 0){

        ?>
        <div class="column-30 w-col w-col-6">
          <img src="<?php echo $fotoEquipe ?> ">
          <h1 class="heading-3"><?php echo get_the_title() ?></h1>
          <h1 class="heading-19"><?php echo $equipe_cargo = rwmb_meta('Sumera_equipe_cargo'); ?></h1>
          <p class="paragraph-2"><?php echo get_the_content() ?></p>
          <?php if ($equipe_cargo = rwmb_meta('Sumera_equipe_redeSolcial')):?>
          <a href="<?php echo $equipe_cargo = rwmb_meta('Sumera_equipe_redeSolcial'); ?>" target="_blank">
            <img src="<?php bloginfo('template_directory'); ?>/images/in-icon.png" class="image-5">
          </a>
        <?php endif; ?>
        </div>
        <?php }else if($i == 1){ ?>
        <div class="column-31 w-col w-col-6">
          <img src="<?php echo $fotoEquipe ?> ">
          <h1 class="heading-3"><?php echo get_the_title() ?></h1>
          <h1 class="heading-19"><?php echo $equipe_cargo = rwmb_meta('Sumera_equipe_cargo'); ?></h1>
          <p class="paragraph-2"><?php echo get_the_content() ?></p>
          <?php if ($equipe_cargo = rwmb_meta('Sumera_equipe_redeSolcial')):?>
          <a href="<?php echo $equipe_cargo = rwmb_meta('Sumera_equipe_redeSolcial'); ?>" target="_blank">
            <img src="<?php bloginfo('template_directory'); ?>/images/in-icon.png" class="image-6">
          </a>
        <?php endif; ?>
        </div>
        <?php }else if($i == 2){ ?>
        <div class="column-32 w-col w-col-6">
          <img src="<?php echo $fotoEquipe ?> ">
          <h1 class="heading-3"><?php echo get_the_title() ?></h1>
          <h1 class="heading-19"><?php echo $equipe_cargo = rwmb_meta('Sumera_equipe_cargo'); ?></h1>
          <p class="paragraph-2"><?php echo get_the_content() ?></p>
          <?php if ($equipe_cargo = rwmb_meta('Sumera_equipe_redeSolcial')):?>
          <a href="<?php echo $equipe_cargo = rwmb_meta('Sumera_equipe_redeSolcial'); ?>" target="_blank">
            <img src="<?php bloginfo('template_directory'); ?>/images/in-icon.png" class="image-7">
          </a>
        <?php endif; ?>
        </div>
         <?php };$i++; endwhile; wp_reset_query(); ?>
      </div>
    </div>
  </div>

  <div class="section-projetos">
    <div class="div-features">
      <h1 class="heading-7">Conheça nossos projetos!</h1><a href="<?php echo home_url('/projetos/'); ?>" class="button-2 w-button">Projetos</a></div>
  </div>

<?php get_footer(); ?>