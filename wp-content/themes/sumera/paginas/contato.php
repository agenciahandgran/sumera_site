<?php
/**
 * Template Name: Contato
 * Description: Página Contato
 *
 * @package Sumera
 */

global $configuracao;

get_header(); ?>
<div class="section-contato">
    <div class="div-contato">
      <h1>Entre em contato</h1>
      <div class="w-form">

        <?php echo do_shortcode('[contact-form-7 id="12" title="Formulário de contato"]'); ?>
      

        <div class="w-form-done">
          <div>Thank you! Your submission has been received!</div>
        </div>
        <div class="w-form-fail">
          <div>Oops! Something went wrong while submitting the form.</div>
        </div>
      </div>
    </div>
  </div>
  <div class="section-10">
    <div class="div-block-11">
      <div class="row-16 w-row">
        <div class="column-41 w-col w-col-6">
           <div class="contatosInfos">
           <?php 
              $telefones = explode(",", $configuracao['telefones']);
              foreach ($telefones  as $telefones ):
                $telefones = $telefones;
            ?>
            <div class="row-15 w-row">
              <div class="w-col w-col-1">
                <img src="<?php bloginfo('template_directory'); ?>/images/phone.png">
              </div>
              <div class="w-col w-col-11">
                <h1 class="heading-24"><?php echo $telefones  ?></h1>
              </div>
            </div>

            <?php endforeach; ?>

            <div class="row-15 w-row">
              <div class="w-col w-col-1"><img src="<?php bloginfo('template_directory'); ?>/images/contato-icon.png"></div>
              <div class="w-col w-col-11">
                <h1 class="heading-24"><?php echo $configuracao['email'] ?></h1>
              </div>
            </div>

            <div class="row-15 w-row">
              <div class="w-col w-col-1"><img src="<?php bloginfo('template_directory'); ?>/images/location-icon.png"></div>
              <div class="w-col w-col-11">
                <h1 class="heading-24"><?php echo $configuracao['endereco'] ?></h1>
              </div>
            </div>
          </div>
        </div>

        <div class="column-40 w-col w-col-6">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3602.420531359281!2d-49.312878484901425!3d-25.457629683775668!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce3a416133ce9%3A0xc4185e84f65f5804!2sR.+Dom+Orione%2C+257+-+Santa+Quiteria%2C+Curitiba+-+PR%2C+80310-250!5e0!3m2!1spt-BR!2sbr!4v1516891129040" width="100%" height="460" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </div>
<?php get_footer(); ?>