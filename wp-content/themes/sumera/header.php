<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Sumera
 */
global $configuracao;
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	           <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TH54KHW');</script>
<!-- End Google Tag Manager -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	 <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php bloginfo('template_directory'); ?>/favicon_old.png" rel="shortcut icon" type="image/x-icon">
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>

 <link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/favicon_old.png">



	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TH54KHW"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php if (get_the_title() == "Inicial"): ?>

<div class="hero">
	<div data-collapse="all" data-animation="default" data-duration="400" class="navbar-2 w-nav">
		<div class="container1">
			
			<div class="logo topoLogo">
				<a href="<?php echo home_url('/'); ?>" class="w-inline-block"><img src="<?php echo $configuracao['opt_logo']['url'] ?>"></a>
			</div>
			<button class="btnAbrirmenu">
				<span></span>
				<span></span>
				<span></span>
			</button>
			<nav role="" class="navMenu">
				<a href="<?php echo home_url('/empresa/'); ?>" class="nav-link w-nav-link">Empresa</a>
				<a href="<?php echo home_url('/servicos/'); ?>" class="nav-link w-nav-link">Serviços</a>
				<a href="<?php echo home_url('/projetos/'); ?>" class="nav-link w-nav-link">Projetos</a>
				<a href="<?php echo home_url('/contato/'); ?>" class="nav-link w-nav-link">Contato</a>
			</nav>
			<div class="logo bottomLogo">
				<a href="<?php echo home_url('/'); ?>" class="w-inline-block"><img src="<?php echo $configuracao['opt_logo']['url'] ?>"></a>
			</div>
		</div>
	</div>

		<?php 
			//LOOP DE POST DESTAQUES
			$posDestaques = new WP_Query( array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => 1) );
			while ( $posDestaques->have_posts() ) : $posDestaques->the_post();
				$fotoDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				$fotoDestaque = $fotoDestaque[0];
		 ?>
		<div class="container-img-header">
			<div class="container-box-header-text">
				<div class="base-box">
					<h1 class="heading-home"><?php echo get_the_title() ?></h1>
					<h2 class="heading-2-home"><?php echo get_the_content() ?></h2>
				</div>
			</div>
		</div>
		<?php endwhile; wp_reset_query(); ?>

</div>

<?php else: ?>


	<div class="hero-2">
		<div data-collapse="all" data-animation="default" data-duration="400" class="navbar-2 w-nav">
			<div class="container1">
				<div class="logo topoLogo">
					<a href="<?php echo home_url('/'); ?>" class="w-inline-block"><img src="<?php echo $configuracao['opt_logo']['url'] ?>"></a>
				</div>
				<button class="btnAbrirmenu">
					<span></span>
					<span></span>
					<span></span>
				</button>
				<nav role="" class="navMenu">
					<a href="<?php echo home_url('/empresa/'); ?>" class="nav-link w-nav-link">Empresa</a>
					<a href="<?php echo home_url('/servicos/'); ?>" class="nav-link w-nav-link">Serviços</a>
					<a href="<?php echo home_url('/projetos/'); ?>" class="nav-link w-nav-link">Projetos</a>
					<a href="<?php echo home_url('/contato/'); ?>" class="nav-link w-nav-link">Contato</a>
				</nav>
				<div class="logo bottomLogo">
					<a href="<?php echo home_url('/'); ?>" class="w-inline-block"><img src="<?php echo $configuracao['opt_logo']['url'] ?>"></a>
				</div>
			</div>
		</div>
	</div>

<?php endif; ?>

<script>
	$( ".btnAbrirmenu" ).click(function() {
	  $(".navMenu").slideToggle();
	});
</script>