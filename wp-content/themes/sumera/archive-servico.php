<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Sumera
 */

get_header(); ?>

	<div class="section-header-nossos-servicos">
    <div class="div-header-projetos">
      <div class="row-11 w-row">
        <div class="column-35 w-col w-col-6">
         <?php echo $configuracao['servico_texto_Descricao'] ?>
        </div>
        <div class="w-col w-col-6">
          <img src="<?php echo $configuracao['img_servico_ilustrativa']['url'] ?>" srcset="<?php echo $configuracao['img_servico_ilustrativa']['url'] ?>, <?php echo $configuracao['img_servico_ilustrativa']['url'] ?> 602w" sizes="(max-width: 479px) 100vw, (max-width: 767px) 47vw, 48vw">
        </div>
      </div>
    </div>
  </div>
 <?php 
    $i = 0;
    //LOOP DE POST Servico
    $custonServicos = new WP_Query( array( 'post_type' => 'servico', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
    while ( $custonServicos->have_posts() ) : $custonServicos->the_post();
        $fotoServicos = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
        $fotoServicos = $fotoServicos[0];
        $servico_icone = rwmb_meta('Sumera_servico_icone');
    if ($i % 2 == 0):

  ?>
  <div class="section-servicos">
    <div class="div-servicos">
      <div class="row-12 w-row">
        <div class="column-37 w-col w-col-6"><img src="<?php echo $fotoServicos ?>" srcset="<?php echo $fotoServicos ?> 500w, <?php echo $fotoServicos ?> 555w" sizes="(max-width: 555px) 100vw, (max-width: 767px) 555px, 50vw"></div>
        <div class="column-36 w-col w-col-6">
          <div class="row-13 w-row">
            <div class="w-col w-col-2">
              <?php foreach ( $servico_icone  as  $servico_icone): $servico_icone  = $servico_icone['full_url']; ?>
              <img src="<?php echo $servico_icone ?>">
              <?php endforeach; ?>
            </div>
            <div class="column-38 w-col w-col-10">
              <h1 class="heading-22"><?php echo get_the_title() ?></h1>
            </div>
          </div>
          <p class="paragraph"><?php echo get_the_content() ?></p>
          <a href="<?php echo home_url('contato/'); ?>" class="button w-button">Entre em contato</a>
        </div>
      </div>
    </div>
  </div>
  <?php else: ?>
    <div class="section-service-2">
      <div class="div-servicos-2">
        <div class="w-row">
          <div class="column-39 w-col w-col-6">
            <div class="row-14 w-row">
              <div class="w-col w-col-2">
                <?php foreach ( $servico_icone  as  $servico_icone): $servico_icone  = $servico_icone['full_url']; ?>
                <img src="<?php echo $servico_icone ?>">
                <?php endforeach; ?>
              </div>
              <div class="w-col w-col-10">
                <h1 class="heading-22"><?php echo get_the_title() ?></h1>
              </div>
            </div>
            <p class="paragraph"><?php echo get_the_content() ?></p>
            <a href="<?php echo home_url('/contato/'); ?>" class="button w-button">Entre em contato</a>
          </div>
          <div class="w-col w-col-6">
            <img src="<?php echo $fotoServicos ?>" srcset="<?php echo $fotoServicos ?> 500w, <?php echo $fotoServicos ?> 555w" sizes="(max-width: 555px) 100vw, (max-width: 767px) 555px, 50vw">
          </div>
        </div>
      </div>
    </div>
   <?php endif;$i++; endwhile; wp_reset_query(); ?>

    <!-- INFORMATIVO -->
     <?php if ($configuracao['img_informativo']['url']):?>
  <div class="section-4" style="background:url(<?php echo $configuracao['img_informativo']['url'] ?>)">
    <div class="container1-caixa">
      <div class="row-2 w-row">
        <div class="w-col w-col-8">
          <h1 class="heading-9" style="display: none">O sonho da casa própria<br>ao seu alcance.</h1>
          <h1 class="heading-10" style="display: none">CONSTRUA COM A SUMERA E PAGUE EM ATÉ 360X</h1>
        </div>
        <!-- <div class="w-col w-col-4"><img src="images/logo-caixa-financiamento.png"></div> -->
      </div>
    </div>
  </div>
  <?php endif; ?>
<?php
get_footer();
